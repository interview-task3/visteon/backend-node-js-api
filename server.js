const express = require("express");
const PORT = process.env.PORT || 9000;
const app = express();
const cors = require('cors')
app.use(express.json());
app.use(cors());

const db = require("./src/app/db/conection");

const BOOK_ROUTER = require('./src/app/routes/books')
app.use("/api/books", BOOK_ROUTER);


db.initConnection().then(()=>{
    app.listen(PORT, () => {
        console.log(`Server is running in ${PORT}`);
    });
}).catch((err)=>{
    console.log("Could not start server API", err)
})