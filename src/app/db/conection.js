const mongodb = require("mongodb").MongoClient;
let connection;

module.exports = {
  initConnection: () => {
    return new Promise((resolve, reject) => {
      mongodb.connect(
        "mongodb+srv://admin:admin@cluster0.zdst9.mongodb.net/",
        (err, db) => {
          if (err) {
            reject(err);
          }
          console.log("API started");
          connection = db;
          resolve();
        }
      );
    });
  },

  getConnection: () => {
    return connection;
  },

  getDb: () => {
    return connection.db("visteon");
  },
};