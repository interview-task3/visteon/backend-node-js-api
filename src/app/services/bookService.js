const dao = require("../dao/bookDao")
module.exports = {

    saveBook: async(data) => {
        //let newData = getInKgDetails(data); 
        return await dao.saveBook(data)
      },


    getBooks: async(filter,size, page) => {
    return await dao.getBooks(filter,size, page)
  },
};