const db = require("../db/conection");
const { v4: uuidv4 } = require('uuid');
const _collectionName = 'Book';
module.exports = {
    saveBook: async (data) => {
        data._id = uuidv4();
        data.updatedAt = new Date();
        const collection = db.getDb().collection(_collectionName)
        return await collection.insertOne(data)
    },
    getBooks: async ({ title }, size, page) => {
        let response= {};
        let search = {}
        if (title) {
            search.name = title
        }
        const collection = db.getDb().collection(_collectionName)
        response.data = await collection.find(search).limit(size).skip(size * page).toArray();
        response.totalCount = await collection.count();
        console.log("******************")
        console.log(response)
        console.log("******************")
        return response
    },
};