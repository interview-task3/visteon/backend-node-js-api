const router = require("express").Router();
const service = require("../services/bookService")


router.post("/saveBook", (req, res) => {
    service
      .saveBook(req.body)
      .then((response) => {
        res.status(200).send(response);
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message,
        });
      });
  });
router.get("/getBooks", (req, res) => {

    const title = req.query.title;
    const filter = {
        title
    }
    const parsedSize = parseInt(req.query.size)
    const parsedPage = parseInt(req.query.page)
    const size = !isNaN(parsedSize) ? parsedSize : 0;
    const page = !isNaN(parsedPage) ? parsedPage > 0 ? parsedPage-1 : 0 : 0;
    service
      .getBooks(filter,size, page)
      .then((response) => {
        res.status(200).send(response);
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message,
        });
      });
  });

  module.exports = router;